# [1.9.0](https://gitlab.com/aleia-team/public/allonias3/compare/1.8.0...1.9.0) (2025-01-27)


### Bug Fixes

* **config:** mandatory envvars in test env ([4eb0b75](https://gitlab.com/aleia-team/public/allonias3/commit/4eb0b7528468350061a20c5221ea62fd261fd860))
* **docs:** docstrings ([e66436d](https://gitlab.com/aleia-team/public/allonias3/commit/e66436db45ac562b874f1e89c0e913f2db4b9067))
* **pickle:** return use_extension in __getstate__ ([a13fccd](https://gitlab.com/aleia-team/public/allonias3/commit/a13fccd760c16ed653c9aaac2c1566e7d2034d4d))
* **suffix:** minor fixes ([21e969a](https://gitlab.com/aleia-team/public/allonias3/commit/21e969a7a83424ce6109b801ae7f6b845c272374))


### Features

* **allonias3:** better reporting of error when asking to handle type when packages are missing ([1fc31d9](https://gitlab.com/aleia-team/public/allonias3/commit/1fc31d9aa98ddb7766e3aca83929c47f06dc32f4))
* **config:** clean useless attributes, fix tests ([b42a09c](https://gitlab.com/aleia-team/public/allonias3/commit/b42a09c4b22e5f3593052b10c3abd06963c22ad7))
* **encoder-decoder:** pass S3Path directly, and use use_extension internally ([76aa924](https://gitlab.com/aleia-team/public/allonias3/commit/76aa9246199a663217e5509ae93afb7533089c1f))
* **envvars:** make all envvars mandatory ([7817f48](https://gitlab.com/aleia-team/public/allonias3/commit/7817f48aeae6cd96badc094af2c6f4c77b66b4ab))
* **S3Path:** allow users to bypass file extension detection ([6bcd636](https://gitlab.com/aleia-team/public/allonias3/commit/6bcd636f772a11bb4b11705d6c97d5e25ba9a0bc))
* **use_extension:** more tests ([783ba30](https://gitlab.com/aleia-team/public/allonias3/commit/783ba30cc016a13f46a7b936c56473dc4c2aa694))
* **use_extension:** pytests ([eee1844](https://gitlab.com/aleia-team/public/allonias3/commit/eee18441bb2a16f861c227c2a1366c8fc1ea900f))

# [1.8.0](https://gitlab.com/aleia-team/public/allonias3/compare/1.7.0...1.8.0) (2025-01-15)


### Features

* **doc:** add doc on how to use allonias3 locally ([1a807ec](https://gitlab.com/aleia-team/public/allonias3/commit/1a807ec784cd07fdcf95f4b1cabcbbe5624acada))

# [1.7.0](https://gitlab.com/aleia-team/public/allonias3/compare/1.6.0...1.7.0) (2025-01-14)


### Features

* **alloniaconfigs:** use latest ([f8a2532](https://gitlab.com/aleia-team/public/allonias3/commit/f8a2532dd3416e70026d7ff064dc95733bfd5fd4))

# [1.6.0](https://gitlab.com/aleia-team/public/allonias3/compare/1.5.2...1.6.0) (2025-01-10)


### Features

* **base_path:** added the get_sheets method ([8898fe6](https://gitlab.com/aleia-team/public/allonias3/commit/8898fe601616d25921355ba31b0ca5cd7e5f85ca))

## [1.5.2](https://gitlab.com/aleia-team/public/allonias3/compare/1.5.1...1.5.2) (2024-12-05)


### Bug Fixes

* **append:** appending with minio was missing a cast to BytesIO ([f25db63](https://gitlab.com/aleia-team/public/allonias3/commit/f25db63dd6ff2d451bc0dc648815d0177b2221bd))

## [1.5.1](https://gitlab.com/aleia-team/public/allonias3/compare/1.5.0...1.5.1) (2024-11-06)


### Bug Fixes

* **path:** use self.path instead of self._path in itruediv and reset object_type when setting self.path ([bffdfeb](https://gitlab.com/aleia-team/public/allonias3/commit/bffdfebe28e50a395959357e2a472c63224904cd))
* **type:** reset object_type when using itruediv ([06e61be](https://gitlab.com/aleia-team/public/allonias3/commit/06e61bea441278352809a3ecee5e2d48bdc7f3a2))

# [1.5.0](https://gitlab.com/aleia-team/public/allonias3/compare/1.4.2...1.5.0) (2024-10-25)


### Features

* **error:** print full stack trace when warning about an error ([f9e941f](https://gitlab.com/aleia-team/public/allonias3/commit/f9e941fe74ee3089cda67729d406bf9769b13740))

## [1.4.2](https://gitlab.com/aleia-team/public/allonias3/compare/1.4.1...1.4.2) (2024-10-24)


### Bug Fixes

* **ci:** allow check release to fail ([450b1ab](https://gitlab.com/aleia-team/public/allonias3/commit/450b1abd881383581a9bcad5994bedca28d6b13d))

## [1.4.1](https://gitlab.com/aleia-team/public/allonias3/compare/1.4.0...1.4.1) (2024-10-24)


### Bug Fixes

* **head:** removed 'metadata' propety, flattened "head" response between minio and boto ([b74ef9c](https://gitlab.com/aleia-team/public/allonias3/commit/b74ef9ca240fa8adf54739d07b9f03ab0d75d001))

# [1.4.0](https://gitlab.com/aleia-team/public/allonias3/compare/1.3.4...1.4.0) (2024-10-24)


### Features

* **config:** access user_id through user_arn if available ([a2ec4f9](https://gitlab.com/aleia-team/public/allonias3/commit/a2ec4f97f33a821341aa44ebfbf0cc11baecebbd))
* **config:** access user_id through user_arn if available ([048eef1](https://gitlab.com/aleia-team/public/allonias3/commit/048eef1dc0eb664a2e09c5549a6ed8b4c96e0262))

## [1.3.4](https://gitlab.com/aleia-team/public/allonias3/compare/1.3.3...1.3.4) (2024-10-16)


### Bug Fixes

* **dir:** fix is_dir and rmdir behavior relative to root bucket, fixed logs ([974543c](https://gitlab.com/aleia-team/public/allonias3/commit/974543cf5f6649c5306bf254f8340784c7357e9d))

## [1.3.3](https://gitlab.com/aleia-team/public/allonias3/compare/1.3.2...1.3.3) (2024-10-10)


### Bug Fixes

* **configs:** s3_proxy_url can be None without error ([bdeb3ec](https://gitlab.com/aleia-team/public/allonias3/commit/bdeb3ecbf88a465de21a81eeeea2b1e683ccf460))

## [1.3.2](https://gitlab.com/aleia-team/public/allonias3/compare/1.3.1...1.3.2) (2024-10-10)


### Bug Fixes

* **up/dl:** simplify dl and upload ([22ce767](https://gitlab.com/aleia-team/public/allonias3/commit/22ce76742eef3057d3f98288ecd8387d667b6784))

## [1.3.1](https://gitlab.com/aleia-team/public/allonias3/compare/1.3.0...1.3.1) (2024-10-10)


### Bug Fixes

* **logs:** add nb_log function ([0572c20](https://gitlab.com/aleia-team/public/allonias3/commit/0572c208446cc46028d6d859726b30b3e9643a99))
* **logs:** add nb_log function ([a4451b7](https://gitlab.com/aleia-team/public/allonias3/commit/a4451b78a5867c47af93ec17aadefe4eacffec83))

# [1.3.0](https://gitlab.com/aleia-team/public/allonias3/compare/1.2.0...1.3.0) (2024-10-10)


### Features

* **logs:** add nb_log function ([35df1b8](https://gitlab.com/aleia-team/public/allonias3/commit/35df1b82a26a214d4fef6c56f1a66bbe057b5f13))

# [1.2.0](https://gitlab.com/aleia-team/public/allonias3/compare/1.1.0...1.2.0) (2024-09-30)


### Features

* **csv:** can read/write with user-defined encodings ([d7ba7af](https://gitlab.com/aleia-team/public/allonias3/commit/d7ba7aff4bb4f4d8ccc498a46f644d6e7d67b710))

# [1.1.0](https://gitlab.com/aleia-team/public/allonias3/compare/1.0.8...1.1.0) (2024-09-13)


### Features

* make S3Path itself (un)picklable ([c0adef6](https://gitlab.com/aleia-team/public/allonias3/commit/c0adef63a72dafc6f4571e64491bda2bd95c4a90))

## [1.0.8](https://gitlab.com/aleia-team/public/allonias3/compare/1.0.7...1.0.8) (2024-09-13)


### Bug Fixes

* excel decoder ([383decc](https://gitlab.com/aleia-team/public/allonias3/commit/383deccc1f8c2f65e00a9f3e753b61ff9f8ac73f))

## [1.0.7](https://gitlab.com/aleia-team/public/allonias3/compare/1.0.6...1.0.7) (2024-09-12)


### Bug Fixes

* import_from_s3 supports '-' in names ([12cd3e3](https://gitlab.com/aleia-team/public/allonias3/commit/12cd3e3c0401da1e8defc2df0029d6528188823a))

## [1.0.6](https://gitlab.com/aleia-team/public/allonias3/compare/1.0.5...1.0.6) (2024-09-10)


### Bug Fixes

* json support ([3768449](https://gitlab.com/aleia-team/public/allonias3/commit/37684492593f9aa5b7b7fb7e576cfe513fa46be8))

## [1.0.5](https://gitlab.com/aleia-team/public/allonias3/compare/1.0.4...1.0.5) (2024-09-10)


### Bug Fixes

* PROJET_ID and USER_ID in config ([a73e4a2](https://gitlab.com/aleia-team/public/allonias3/commit/a73e4a28894caa2a0601c96156b8b495541ba08a))

## [1.0.4](https://gitlab.com/aleia-team/public/allonias3/compare/1.0.3...1.0.4) (2024-09-09)


### Bug Fixes

* add getattr_safe_property on classproperties ([39c6637](https://gitlab.com/aleia-team/public/allonias3/commit/39c663796e859c8f6a632a5e72e8fe913905e9df))

## [1.0.3](https://gitlab.com/aleia-team/public/allonias3/compare/1.0.2...1.0.3) (2024-05-22)


### Bug Fixes

* convert columns to str before appending dataframes ([2a901b7](https://gitlab.com/aleia-team/public/allonias3/commit/2a901b71dcc4219bf723d95beaf2e6a80d17e25e))

## [1.0.2](https://gitlab.com/aleia-team/public/allonias3/compare/1.0.1...1.0.2) (2024-05-22)


### Bug Fixes

* correct type checking ([8b6f592](https://gitlab.com/aleia-team/public/allonias3/commit/8b6f592fadb9a90f89e553a34a9240a77b206db5))

## [1.0.1](https://gitlab.com/aleia-team/public/allonias3/compare/1.0.0...1.0.1) (2024-05-15)


### Bug Fixes

* pages job ([0aeb1a7](https://gitlab.com/aleia-team/public/allonias3/commit/0aeb1a79362ee1d87b62ee92fc35cde0f8f4d93d))
* remove useless CI job ([3639afe](https://gitlab.com/aleia-team/public/allonias3/commit/3639afe9fd5441f3d1c4c56c9b4795bd5a906b80))

# 1.0.0 (2024-05-15)


### Features

* import CI caching ([27f6d11](https://gitlab.com/aleia-team/public/allonias3/commit/27f6d11ce85809546f031eb0cf8a6095d28899e9))
* python3.9 ([d113f23](https://gitlab.com/aleia-team/public/allonias3/commit/d113f2373de7a734940dd768ede19000d385dcb9))
* testing no data type handeling ([b3f52a4](https://gitlab.com/aleia-team/public/allonias3/commit/b3f52a45a794b02c3fc88f0a2354851ef2545297))
