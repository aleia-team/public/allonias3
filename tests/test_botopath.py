import pytest

from .s3path_testclass import TestS3Path


@pytest.mark.usefixtures("_boto_buckets")
class TestBotoPath(TestS3Path):
    CLIENT_TYPE = "boto"
