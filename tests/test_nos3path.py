from pathlib import Path

import pytest
from expects import be_none, be_true, equal, expect

from allonias3 import Configs, S3Path


class TestBasePath:
    filepath = "notebooks/dataset/file.csv"
    dirpath = "notebooks/dataset"
    dirpath_with_slash = "notebooks/dataset/"

    def test_empty_init(self):
        path = S3Path(self.filepath)
        expect(path.persistent).to(be_true)
        expect(path.verbose).to(be_true)
        expect(path.handle_type).to(be_true)
        expect(path._object_type).to(be_none)

    @pytest.mark.parametrize(
        (
            "s3path",
            "expected_s3path",
            "persistent",
            "verbose",
            "handle_type",
            "object_type",
        ),
        [
            (None, "", True, True, True, None),
            ("", "", True, True, True, None),
            ("/", "", True, True, True, None),
            (".", "", True, True, True, None),
            ("filepath", "filepath", True, True, True, None),
            ("dirpath", "dirpath", False, False, False, "dataset"),
            ("dirpath_with_slash", "dirpath", False, False, False, "dataset"),
        ],
    )
    def test_init(
        self,
        s3path,
        expected_s3path,
        persistent,
        verbose,
        handle_type,
        object_type,
    ):
        path = S3Path(
            getattr(self, s3path)
            if s3path and hasattr(self, s3path)
            else s3path,
            verbose=verbose,
            persistent=persistent,
            handle_type=handle_type,
            object_type=object_type,
        )
        expected = (
            getattr(self, expected_s3path)
            if expected_s3path and hasattr(self, expected_s3path)
            else expected_s3path
        )
        expect(path.path).to(equal(Path(expected)))
        expect(path._path).to(equal(Path(f"/{expected}")))

        expect(path.persistent).to(equal(persistent))
        expect(path.verbose).to(equal(verbose))
        expect(path.handle_type).to(equal(handle_type))
        expect(path._object_type).to(equal(object_type))

    @pytest.mark.parametrize(
        ("path", "object_type", "before_call", "after_call"),
        [
            (None, None, None, "unknown"),
            (None, "unknown", "unknown", "unknown"),
            (None, "dataset", "dataset", "dataset"),
            (None, "datasett", "", ""),
            ("notebooks/model", "unknown", "unknown", "unknown"),
            ("notebooks/model", "dataset", "dataset", "dataset"),
            ("notebooks/model", "datasett", "", ""),
            ("notebooks/model", None, None, "model"),
            ("notebooks/notebook", None, None, "notebook"),
            ("notebooks/dataset", None, None, "dataset"),
            ("notebooks/notebook/.s3keep", None, None, "s3keep"),
            ("workflows", None, None, "workflows"),
            ("applications/coucou", None, None, "unknown"),
            ("applications/module_coucou", None, None, "job"),
            ("applications/user-service_coucou", None, None, "service"),
            ("jobs", None, None, "log"),
            (".config", None, None, "package"),
            (".logs", None, None, "log"),
        ],
    )
    def test_object_type(self, path, object_type, before_call, after_call):
        if object_type == "datasett":
            with pytest.raises(TypeError):
                S3Path(path, object_type=object_type)
            path = S3Path()
            with pytest.raises(TypeError):
                path.object_type = object_type
        else:
            path = S3Path(path, object_type=object_type)
            expect(path._object_type).to(equal(before_call))
            expect(path.object_type).to(equal(after_call))

    def test_useful_pathlib_attributes(self):
        path = S3Path("notebooks/dataset/test.csv")
        expect(path.parent).to(equal(S3Path("notebooks/dataset")))
        expect(path.parts).to(equal(["notebooks", "dataset", "test.csv"]))
        expect(path.parents).to(
            equal([S3Path("notebooks/dataset"), S3Path("notebooks"), S3Path()])
        )
        expect(path.name).to(equal("test.csv"))
        expect(path.stem).to(equal("test"))
        expect(path.suffix).to(equal(".csv"))

    def test_kwargs(self):
        path = S3Path("notebooks/dataset/test.csv")
        expect(path.kwargs).to(
            equal(
                {
                    "persistent": True,
                    "verbose": True,
                    "encoding": "utf-8",
                    "handle_type": True,
                }
            )
        )

    def test_bucket(self):
        expect(S3Path(persistent=False).bucket).to(
            equal(Configs.instance.non_persistent_bucket_name)
        )
        expect(S3Path().bucket).to(
            equal(Configs.instance.persistent_bucket_name)
        )

    def test_bool(self):
        path = S3Path()
        if path:
            raise AssertionError
        path = S3Path("chien")
        if not path:
            raise AssertionError

    def test_str(self):
        expect(str(S3Path())).to(equal(""))
        expect(str(S3Path("chien"))).to(equal("chien"))
        expect(str(S3Path("/chien"))).to(equal("chien"))
        expect(str(S3Path("chien/"))).to(equal("chien"))

    def test_fspath(self):
        expect(S3Path("chien").__fspath__()).to(
            equal(f"s3://{Configs.instance.persistent_bucket_name}/chien")
        )

    def test_div(self):
        path = S3Path()
        expect(path / "chien").to(equal(S3Path("chien")))
        path /= "chien"
        expect(path).to(equal(S3Path("chien")))

    def test_equal(self):
        expect(S3Path("/chien")).to(equal(S3Path("chien")))
        expect(S3Path("chien/")).to(equal(S3Path("chien")))
        expect(S3Path("chat")).not_to(equal(S3Path("chien")))

    def test_suffix(self):
        path = S3Path("chien/test.csv")
        expect(path.with_suffix(".txt")).to(equal(S3Path("chien/test.txt")))
        expect(path.with_suffix("txt")).to(equal(S3Path("chien/test.txt")))
        path.change_suffix(".txt")
        expect(path).to(equal(S3Path("chien/test.txt")))
