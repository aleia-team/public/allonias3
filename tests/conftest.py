import shutil
from pathlib import Path

import boto3
import moto
import pytest
from minio.versioningconfig import ENABLED, VersioningConfig
from pytest_minio_mock import minio_mock, minio_mock_servers  # noqa: F401
from pytest_minio_mock.plugin import MockMinioClient

from allonias3 import Configs, S3Path, nb_log
from allonias3.minio.helpers import Minio

nb_log("INFO")

MockMinioClient.custom_remove_objects = Minio.custom_remove_objects
MockMinioClient.custom_remove_object = Minio.custom_remove_object
MockMinioClient.custom_list_objects = Minio.custom_list_objects


def make_boto_buckets(client):
    client.create_bucket(Bucket=Configs.instance.persistent_bucket_name)
    client.put_bucket_versioning(
        Bucket=Configs.instance.persistent_bucket_name,
        VersioningConfiguration={"Status": "Enabled"},
    )
    client.create_bucket(Bucket=Configs.instance.non_persistent_bucket_name)


def delete_boto_buckets(client):
    s3 = boto3.resource("s3")
    client.put_bucket_versioning(
        Bucket=Configs.instance.persistent_bucket_name,
        VersioningConfiguration={"Status": "Disabled"},
    )
    for bucket in (
        Configs.instance.persistent_bucket_name,
        Configs.instance.non_persistent_bucket_name,
    ):
        s3.Bucket(bucket).objects.all().delete()

    client.delete_bucket(Bucket=Configs.instance.persistent_bucket_name)
    client.delete_bucket(Bucket=Configs.instance.non_persistent_bucket_name)


def make_minio_buckets(client):
    client.make_bucket(Configs.instance.persistent_bucket_name)
    client.set_bucket_versioning(
        Configs.instance.persistent_bucket_name, VersioningConfig(ENABLED)
    )
    client.make_bucket(Configs.instance.non_persistent_bucket_name)


def delete_minio_buckets(client):
    client.buckets = {}


@pytest.fixture
def _skipabstract(request):
    if request.cls and request.cls.__name__ == "TestS3Path":
        pytest.skip("Skipped abstract class")


@pytest.fixture
def _clean(_skipabstract):
    if S3Path.CLIENT_TYPE == "boto":
        client = boto3.client("s3", endpoint_url=None)
        delete_boto_buckets(client)
        make_boto_buckets(client)
    else:
        delete_minio_buckets(S3Path.client)
        make_minio_buckets(S3Path.client)
    yield
    for path in (
        "localfile",
        "test.txt",
        "localdir",
        "notebooks",
        "localfile2",
        "to_import.py",
        "dir_to_import",
    ):
        if (localpath := Path(path)).is_file():
            localpath.unlink()
        if localpath.is_dir():
            shutil.rmtree(str(localpath))


@pytest.fixture
def upload():
    return "to_import.py"


@pytest.fixture
def _boto_buckets():
    mock = moto.mock_aws()
    mock.start()
    client = boto3.client(
        "s3",
        # It is a pain in the ass to use an endpoint with Moto
        endpoint_url=None,
    )
    make_boto_buckets(client)
    yield
    mock.stop()  # will clean up the buckets


@pytest.fixture
def _minio_buckets(minio_mock):  # noqa: ARG001, F811
    s3_proxy_url = (
        str(Configs.instance.S3_PROXY_URL)
        .replace("https://", "")
        .replace("http://", "")
        .rstrip("/")
    )
    client = Minio(endpoint=s3_proxy_url)
    make_minio_buckets(client)
    yield
    delete_minio_buckets(client)


@pytest.fixture
def make_file(_clean):
    path = S3Path("notebooks/dataset/test.txt")
    response = path.write("coucou")
    return path, response.version_id


@pytest.fixture()
def make_dir(_clean):
    path = S3Path("notebooks/dataset")
    path.mkdir()
    return path


@pytest.fixture()
def prepare_test_sheets(make_dir):
    source_path = "tests/data/file.ods"

    suffixes = (
        ".xls",
        ".xlsx",
        ".xlsm",
        ".xlsb",
        ".odf",
        ".ods",
        ".odt",
    )

    to_return = []

    for suffix in suffixes:
        path = make_dir / f"file{suffix}"
        path.upload(source_path)
        to_return.append(path)
    return to_return
