import os
import sys
from pathlib import Path
from time import sleep

import pytest
from expects import (
    be_false,
    be_none,
    be_true,
    contain,
    equal,
    expect,
    have_len,
)

from allonias3 import Configs, S3Path
from allonias3.helpers.responses import (
    DeleteResponse,
    VersionsResponse,
    WriteResponse,
)


@pytest.mark.usefixtures("_skipabstract")
class TestS3Path:
    CLIENT_TYPE = None

    def test_wrong_bucket(self):
        conf = Configs()
        _bucket = conf.BUCKET_NAME
        conf.BUCKET_NAME = "wrong"
        with pytest.raises(NotADirectoryError):
            S3Path._initialize()
        conf.BUCKET_NAME = _bucket

    def test_existing_type(self, make_file):
        path, _ = make_file
        path._check_existing_type()
        path = S3Path(path, object_type="model")
        with pytest.raises(TypeError):
            path._check_existing_type()
        path.rm(permanently=False)
        path._check_existing_type()
        path.rm(permanently=True)
        path._check_existing_type()

    def test_client(self):
        expect(S3Path.CLIENT_TYPE).to(equal(self.CLIENT_TYPE))
        c1 = S3Path.client
        os.environ["USER_TOKEN_ID"] = "3d8f116e06586b0a"  # noqa: S105
        Configs.reset()
        expect(c1).not_to(equal(S3Path.client))

    def test_versioned(self):
        expect(S3Path(persistent=False).versioned).to(be_false)
        expect(S3Path().versioned).to(be_true)

    def test_persistent(self):
        expect(S3Path(persistent=False).persistent).to(be_false)
        expect(S3Path().persistent).to(be_true)

    @pytest.mark.parametrize(
        ("include_deleted", "details", "make_many"),
        [
            (False, False, False),
            (False, True, False),
            (True, False, False),
            (True, True, False),
            (False, False, True),
            (False, True, True),
            (True, False, True),
            (True, True, True),
        ],
    )
    def test_versions_and_rm(  # noqa: PLR0915, PLR0912
        self, make_file, include_deleted, details, make_many
    ):
        path, version_id = make_file
        expected_simple = [version_id]

        no_versioned = path.__class__(str(path), False)
        expect(no_versioned.versions().successes).to(have_len(0))

        expected_full = VersionsResponse({}, not include_deleted)
        expected_full.successes = [
            {
                "object_name": str(path),
                "last_modified": path.head().last_modified,
                "version_id": version_id,
                "is_latest": True,
                "is_delete_marker": False,
            }
        ]

        if make_many:
            expected_full.successes[0]["is_latest"] = False
            # Create a second version
            expected_simple.insert(0, path.write("coucou").version_id)
            expected_full.successes.insert(
                0,
                {
                    "object_name": str(path),
                    "last_modified": path.head().last_modified,
                    "version_id": expected_simple[0],
                    "is_latest": True,
                    "is_delete_marker": False,
                },
            )
            # Create a third version
            expected_simple.insert(0, path.write("coucou").version_id)
            expected_full.successes[0]["is_latest"] = False
            expected_full.successes.insert(
                0,
                {
                    "object_name": str(path),
                    "last_modified": path.head().last_modified,
                    "version_id": expected_simple[0],
                    "is_latest": True,
                    "is_delete_marker": False,
                },
            )
        versions = path.versions(include_deleted, details)
        if details:
            expect(versions.successes).to(have_len(3 if make_many else 1))
            expect(versions.errors).to(have_len(0))
            expect(versions).to(equal(expected_full))
        else:
            expect(versions).to(have_len(3 if make_many else 1))
            expect(versions).to(equal(expected_simple))
        if make_many:
            expected_full.successes[0]["is_latest"] = False
            sleep(1)
            # Add a delete marker
            deleted = path.rm(permanently=False)
            expect(deleted.errors).to(have_len(0))
            deleted = deleted.successes[0]["delete_marker_version_id"]
            if include_deleted:
                expected_simple.insert(0, deleted)
                expected_full.successes.insert(
                    0,
                    {
                        "object_name": str(path),
                        "version_id": deleted,
                        "is_latest": True,
                        "is_delete_marker": True,
                    },
                )
            versions = path.versions(include_deleted, details)
            if details:
                expect(versions.successes).to(
                    have_len(4 if include_deleted else 3)
                )
                expect(versions.errors).to(have_len(0))
                for i, element in enumerate(versions.successes):
                    if include_deleted and i == 0:
                        expect(element.pop("last_modified")).not_to(be_none)
                    expect(element).to(equal(expected_full.successes[i]))
            else:
                expect(versions).to(have_len(4 if include_deleted else 3))
                expect(versions).to(equal(expected_simple))

            sleep(1)
            # Delete the last delete marker version ID
            deleted_2 = path.rm(version_id=deleted)
            expect(deleted_2.errors).to(have_len(0))
            expect(deleted_2.successes[0]["version_id"]).to(equal(deleted))
            versions = path.versions(include_deleted, details)
            if include_deleted:
                expected_simple.pop(0)
                expected_full.successes.pop(0)
            expected_full.successes[0]["is_latest"] = True
            if details:
                expect(versions.successes).to(have_len(3))
                expect(versions.errors).to(have_len(0))
                expect(versions).to(equal(expected_full))
            else:
                expect(versions).to(have_len(3))
                expect(versions).to(equal(expected_simple))

            sleep(1)
            # Delete the file permanently (3 versions)
            deleted_3 = path.rm()
            expected_delete = DeleteResponse({})
            expected_delete.successes = [
                {
                    "object_name": obj["object_name"],
                    "delete_marker": False,
                    "delete_marker_version_id": None,
                    "version_id": obj["version_id"],
                }
                for obj in expected_full.successes
            ]
            expect(deleted_3).to(equal(expected_delete))
            versions = path.versions(include_deleted, details)
            if details:
                expect(versions.successes).to(have_len(0))
                expect(versions.errors).to(have_len(0))
            else:
                expect(versions).to(have_len(0))

    def test_is_file_is_dir(self, make_dir):
        path_dir = make_dir
        expect(path_dir.is_file()).to(be_false)
        expect(path_dir.is_dir()).to(be_true)
        expect(path_dir.is_dir(check_s3keep=True)).to(be_true)
        expect(path_dir.exists()).to(be_true)
        expect(path_dir.exists(version_id="coucou")).to(be_false)
        path = path_dir / "test.txt"
        expect(path.is_file()).to(be_false)
        expect(path.is_dir()).to(be_false)
        version_id = path.write("").version_id
        expect(path.is_file()).to(be_true)
        expect(path.is_dir()).to(be_false)
        expect(path.is_file(version_id=version_id)).to(be_true)
        expect(path.is_file(version_id="wrong")).to(be_false)
        expect(path.exists(version_id=version_id)).to(be_true)
        expect(path.exists(version_id="wrong")).to(be_false)
        d_version_id = path.rm(permanently=False).successes[0]["version_id"]
        expect(path.is_file()).to(be_false)
        expect(path.is_file(include_deleted=True)).to(be_true)
        expect(path.is_file(version_id=version_id)).to(be_true)
        expect(path.is_file(version_id=d_version_id)).to(be_false)
        path.touch()
        (path_dir / ".s3keep")._delete_all()
        expect(path_dir.is_dir()).to(be_true)
        expect(path_dir.is_dir(check_s3keep=True)).to(be_false)

    def test_size(self, make_file):
        base_size = 6  # word "coucou" is six letters (6 bytes) long
        path, version_id = make_file
        expect(path.size()).to(equal(base_size / 1024**2))
        expect(path.size(unit="kB")).to(equal(base_size / 1024))
        expect(path.size(unit="B")).to(equal(base_size))
        expect(path.size(binary_base=False)).to(equal(base_size / 1000**2))
        expect(path.size(unit="kB", binary_base=False)).to(
            equal(base_size / 1000)
        )
        expect(path.size(version_id=version_id)).to(equal(base_size / 1024**2))

    def test_import_from_s3(self, _clean, upload):
        path = S3Path(f"notebooks/notebook/{upload}")
        expect(path.is_file()).to(be_false)
        path.upload(f"./tests/import_test/dir_to_import/{upload}")
        expect(path.is_file()).to(be_true)
        path.import_from_s3()
        from to_import import coucou

        coucou()

    def test_import_module_from_s3(self, _clean):
        path = S3Path("notebooks/notebook/dir_to_import")
        expect(path.is_dir()).to(be_false)
        path.upload("./tests/import_test/dir_to_import")
        expect(path.is_dir()).to(be_true)
        path.import_from_s3()
        from dir_to_import.to_import import coucou

        coucou()

    @pytest.mark.parametrize(
        ("path", "response"),
        [
            ("", ValueError),
            ("notebooks", PermissionError),
            (".config", PermissionError),
            (".logs", PermissionError),
            ("notebooks/dataset", IsADirectoryError),
            ("notebooks/dataset/test.txt", TypeError),
            (
                "notebooks/model/model.pkl",
                WriteResponse({}),
            ),
        ],
    )
    def test_read_write(self, make_file, path, response):  # noqa: ARG002
        path = S3Path(path, object_type="model")
        if isinstance(response, type) and issubclass(response, Exception):
            with pytest.raises(response):
                path.write("")
        else:
            sleep(0.1)  # sleep to avoid identical creation dates
            write_result = path.write("chien")
            response.object_name = str(path)
            response.bucket_name = path.bucket
            response.version_id = path.head().version_id
            response.params_not_found = []
            expect(write_result.last_modified).not_to(be_none)
            write_result.last_modified = None
            expect(write_result).to(equal(response))
            expect(path.read()).to(equal("chien"))
            sleep(0.1)
            path.write("\nchat", append=True)
            expect(path.read()).to(equal("chien\nchat"))
            expect(path.read(version_id=path.versions()[-1])).to(equal("chien"))
            expect(path.read(revision=1)).to(equal("chien"))
            expect(path.read(version_id=path.versions()[0])).to(
                equal("chien\nchat")
            )
            expect(path.read(revision=2)).to(equal("chien\nchat"))
            path.write(
                Path(__file__).parent.absolute()
                / "import_test"
                / "dir_to_import"
                / "to_import.py"
            )
            expect(path.read(revision=3)).not_to(equal("chien\nchat"))

    def test_content(self, make_file):
        path, _ = make_file
        expect(path.content()).to(equal([]))
        expect(path.parent.content()).to(equal([path]))
        expect(path.parent.parent.content(recursive=False)).to(equal([]))
        expect(path.parent.parent.content(recursive=True)).to(equal([path]))
        expect(path.parent.parent.content(recursive=True, show_hidden=True)).to(
            equal([path])
        )
        expect(
            sorted(
                path.parent.parent.content(
                    recursive=True, show_directories=True
                )
            )
        ).to(equal(sorted([path.parent, path])))
        expect(
            path.parent.parent.content(
                recursive=True, show_files=False, show_directories=True
            )
        ).to(equal([path.parent]))
        expect(
            path.parent.parent.content(recursive=False, show_directories=True)
        ).to(equal([path.parent]))

    def test_download(self, make_file):
        path, version_id = make_file
        path.download("localfile")
        expect(Path("localfile").is_file()).to(be_true)
        path.download()
        expect(Path(path.name).is_file()).to(be_true)
        path.parent.parent.download("localdir")
        expect(Path("localdir").is_dir()).to(be_true)
        expect(Path("localdir/dataset").is_dir()).to(be_true)
        expect(Path("localdir/dataset/test.txt").is_file()).to(be_true)
        path.parent.parent.download()
        expect(Path("notebooks").is_dir()).to(be_true)
        expect(Path("notebooks/dataset").is_dir()).to(be_true)
        expect(Path("notebooks/dataset/test.txt").is_file()).to(be_true)

        path.download("localfile2", version_id=version_id)
        expect(Path("localfile2").is_file()).to(be_true)

    def test_upload(self, make_file):
        path, version_id = make_file
        local_path = Path("toto/tutu/localfile.txt")
        dest_path = S3Path("toto/destination.txt")
        expect(dest_path.is_file()).to(be_false)
        path.download(local_path)

        uploaded = dest_path.upload(local_path)
        expect(uploaded.object_name).to(equal(str(dest_path)))
        expect(dest_path.is_file()).to(be_true)
        expect(dest_path.read()).to(equal("coucou"))

        uploaded2 = dest_path.upload(local_path)
        expect(uploaded2.version_id).not_to(equal(uploaded.version_id))
        with pytest.raises(
            NotADirectoryError,
            match="S3Path points to an existing file but the localpath is a"
            " directory. S3Path must either not exist or be a directory",
        ):
            dest_path.upload(local_path.parent)
        with pytest.raises(
            IsADirectoryError,
            match="S3Path points to an existing directory but the localpath is"
            " a file. S3Path must either not exist or be a file.",
        ):
            dest_path.parent.upload(local_path)

        S3Path("some/dir").upload(local_path.parent)
        expect(S3Path("some/dir").is_dir()).to(be_true)
        expect(S3Path("some/dir/localfile.txt").is_file()).to(be_true)
        S3Path("some/dir").upload(local_path.parent)
        expect(S3Path("some/dir").is_dir()).to(be_true)
        expect(S3Path("some/dir/localfile.txt").is_file()).to(be_true)
        expect(S3Path("some/dir/localfile.txt").versions()).to(have_len(2))

    def test_touch(self, _clean):
        path = S3Path("notebooks/coucou.csv")
        path.touch()
        expect(path.is_file()).to(be_true)

    @pytest.mark.parametrize(
        ("dirname", "failure_type", "failure_message"),
        [
            ("/balounga.s3keep", PermissionError, "Illegal directory name."),
            ("/", None, None),
            ("foo/bar", None, None),
            ("foo", None, None),
        ],
    )
    def test_mkdir(self, _clean, dirname, failure_type, failure_message):
        path = S3Path(dirname)
        if failure_type is not None:
            with pytest.raises(failure_type) as error:
                path.mkdir()
            expect(str(error.value)).to(contain(failure_message))
        else:
            path.mkdir()

    @pytest.mark.parametrize(
        ("dirname", "response", "recursive", "permanently"),
        [
            (
                "inexistant",
                DeleteResponse(
                    {
                        "Errors": [
                            {
                                "Key": "inexistant",
                                "VersionId": None,
                                "Code": "NotADirectoryError",
                                "Message": "Cannot access 'inexistant': No such"
                                " directory",
                            }
                        ]
                    }
                ),
                False,
                False,
            ),
            (
                "notebooks/model",
                DeleteResponse(
                    {
                        "Errors": [
                            {
                                "Key": "notebooks/model",
                                "VersionId": None,
                                "Code": "FileExistsError",
                                "Message": "Directory 'notebooks/model' is not"
                                " empty.",
                            }
                        ]
                    }
                ),
                False,
                False,
            ),
            (
                "notebooks/dataset",
                DeleteResponse(
                    {"Deleted": [{"Key": "notebooks/dataset/.s3keep"}]}
                ),
                False,
                False,
            ),
            (
                "notebooks/model",
                DeleteResponse(
                    {
                        "Deleted": [
                            {"Key": "notebooks/model/.s3keep"},
                            {"Key": "notebooks/model/test.txt"},
                            {"Key": "notebooks/model/test1.txt"},
                            {"Key": "notebooks/model/test2.txt"},
                        ]
                    }
                ),
                True,
                False,
            ),
            (
                "/",
                DeleteResponse(
                    {
                        "Errors": [
                            {
                                "Key": "",
                                "VersionId": None,
                                "Code": "ValueError",
                                "Message": "You did not provide any path to "
                                "delete, or you provided the"
                                " root directory",
                            }
                        ]
                    }
                ),
                True,
                False,
            ),
            (
                "/",
                DeleteResponse(
                    {
                        "Errors": [
                            {
                                "Key": "",
                                "VersionId": None,
                                "Code": "ValueError",
                                "Message": "You did not provide any path to "
                                "delete, or you provided the"
                                " root directory",
                            }
                        ]
                    }
                ),
                False,
                False,
            ),
            (
                "notebooks",
                DeleteResponse(
                    {
                        "Errors": [
                            {
                                "Key": "notebooks",
                                "VersionId": None,
                                "Code": "PermissionError",
                                "Message": "Can not delete 'notebooks'",
                            }
                        ]
                    }
                ),
                False,
                False,
            ),
            (
                "notebooks/dataset",
                DeleteResponse(
                    {"Deleted": [{"Key": "notebooks/dataset/.s3keep"}]}
                ),
                False,
                True,
            ),
            (
                "notebooks/model",
                DeleteResponse(
                    {
                        "Deleted": [
                            {"Key": "notebooks/model/.s3keep"},
                            {"Key": "notebooks/model/test.txt"},
                            {"Key": "notebooks/model/test1.txt"},
                            {"Key": "notebooks/model/test2.txt"},
                        ]
                    }
                ),
                True,
                True,
            ),
        ],
    )
    def test_rmdir(self, make_dir, dirname, response, recursive, permanently):
        path = make_dir
        (path.parent / "model" / "test.txt").touch()
        (path.parent / "model" / "test1.txt").touch()
        (path.parent / "model" / "test2.txt").touch()
        dirname = S3Path(dirname)
        result = dirname.rmdir(recursive=recursive, permanently=permanently)
        if response.successes:
            expect(result.successes).to(have_len(len(response.successes)))
            response = [obj["object_name"] for obj in response.successes]
            for obj in result.successes:
                if permanently:
                    expect(obj["version_id"]).not_to(be_none)
                    expect(obj["delete_marker_version_id"]).to(be_none)
                    expect(obj["delete_marker"]).to(be_false)
                else:
                    expect(obj["version_id"]).to(be_none)
                    if self.__class__.__name__ == "TestBotoPath":
                        # It seems that moto does not include
                        # DeleteMarkerVersionId in delete_objects response
                        continue
                    expect(obj["delete_marker_version_id"]).not_to(be_none)
                    expect(obj["delete_marker"]).to(be_true)
                expect(response).to(contain(obj["object_name"]))
            expect(dirname.is_dir()).to(be_false)

    @pytest.mark.parametrize(
        ("path", "version_id", "permanently", "response"),
        [
            (
                "notebooks/dataset/test.txt",
                None,
                True,
                DeleteResponse(
                    {"Deleted": [{"Key": "notebooks/dataset/test.txt"}]}
                ),
            ),
            (
                "notebooks/dataset/test.txt",
                None,
                False,
                DeleteResponse(
                    {
                        "Deleted": [
                            {
                                "Key": "notebooks/dataset/test.txt",
                                "DeleteMarker": True,
                            }
                        ]
                    }
                ),
            ),
            (
                "notebooks/dataset/test.txt",
                "v0",
                False,
                DeleteResponse(
                    {"Deleted": [{"Key": "notebooks/dataset/test.txt"}]}
                ),
            ),
            (
                "notebooks/dataset/test.txt",
                "v0",
                True,
                DeleteResponse(
                    {"Deleted": [{"Key": "notebooks/dataset/test.txt"}]}
                ),
            ),
            (
                "notebooks/dataset",
                None,
                True,
                DeleteResponse(
                    {
                        "Errors": [
                            {
                                "Key": "notebooks/dataset",
                                "VersionId": None,
                                "Code": "IsADirectoryError",
                                "Message": "Can not delete file"
                                " 'notebooks/dataset': it"
                                " is a directory.",
                            }
                        ]
                    }
                ),
            ),
            (
                "notebooks/dataset/chien",
                None,
                True,
                DeleteResponse(
                    {
                        "Errors": [
                            {
                                "Key": "notebooks/dataset/chien",
                                "VersionId": None,
                                "Code": "NoSuchKey",
                                "Message": "Can not delete file "
                                "'notebooks/dataset/chien'"
                                ": not found.",
                            }
                        ]
                    }
                ),
            ),
            (
                "notebooks/dataset/test.txt",
                "v1",
                True,
                DeleteResponse(
                    {
                        "Errors": [
                            {
                                "Key": "notebooks/dataset/test.txt",
                                "VersionId": "v1",
                                "Code": "NoSuchVersion",
                                "Message": "Can not delete file "
                                "'notebooks/dataset/test.txt'"
                                " with version v1: not such version.",
                            }
                        ],
                    }
                ),
            ),
        ],
    )
    def test_rm(
        self,
        make_file,
        path,
        version_id,
        permanently,
        response,
    ):
        _, version_id_ = make_file
        if version_id == "v0":
            version_id = version_id_
            for obj in response.errors:
                obj["version_id"] = version_id

        path = S3Path(path)
        path2 = S3Path(f"{path}coucou.txt")
        path2.touch()
        rm_response = path.rm(
            version_id=version_id,
            permanently=permanently,
        )
        expect(path2.is_file()).to(be_true)
        if response.successes:
            expect(path.is_file(False, version_id)).to(be_false)
            if response.successes[0].get("delete_marker"):
                delete_version_id = next(
                    obj["version_id"]
                    for obj in S3Path(path)
                    .versions(include_deleted=True, details=True)
                    .successes
                    if obj["is_delete_marker"]
                )
                if version_id:
                    response.successes[0]["version_id"] = delete_version_id
                else:
                    response.successes[0]["delete_marker_version_id"] = (
                        delete_version_id
                    )
            else:
                response.successes[0]["version_id"] = version_id_
        expect(rm_response).to(equal(response))

    def test_url(self, _clean):
        path = S3Path("notebooks/dataset/downloaded.txt")
        url = (
            "https://gitlab.com/aleia-team/public/allonias3/-/raw"
            "/master/.gitignore?ref_type=heads"
        )
        path.write(url)
        expect(path.read()).to(contain(".idea/"))

    def test_head(self, _clean, make_file):
        path, version_id = make_file
        head_response = path.head().to_dict()
        expected = {
            "object_name": str(path),
            "bucket_name": path.bucket,
            "version_id": version_id,
            "size": 6,
            "type": "dataset",
            "author": str(Configs.instance.USER_ID),
        }
        expect(head_response["last_modified"]).not_to(be_none)
        for key in expected:
            expect(expected[key]).to(equal(head_response[key]))
        d_version_id = path.rm(permanently=False).successes[0][
            "delete_marker_version_id"
        ]
        with pytest.raises(Exception, match="Not Found|does not exist"):
            path.head()
        head_response = path.head(version_id=version_id).to_dict()
        expect(head_response["last_modified"]).not_to(be_none)
        for key in expected:
            expect(expected[key]).to(equal(head_response[key]))
        with pytest.raises(
            Exception, match="Method Not Allowed|MethodNotAllowed"
        ):
            path.head(version_id=d_version_id)

    def test_copy(self, make_file):
        path, version_id = make_file
        path.write("test2")
        copy_to = S3Path("notebooks/dataset/copied.txt")
        copied = path.copy(copy_to)
        expect(copied.last_modified).not_to(be_none)
        copied.last_modified = None
        expect(copied).to(
            equal(
                WriteResponse(
                    {
                        "Key": str(copy_to),
                        "Bucket": path.bucket,
                        "VersionId": copy_to.head().version_id,
                        "LastModified": None,
                    }
                )
            )
        )
        expect(copy_to.read()).to(equal("test2"))

        copied = path.copy(copy_to, version_id=version_id)
        copied.last_modified = None
        expect(copied).to(
            equal(
                WriteResponse(
                    {
                        "Key": str(copy_to),
                        "Bucket": path.bucket,
                        "VersionId": copy_to.head().version_id,
                        "LastModified": None,
                    }
                )
            )
        )
        expect(copy_to.read()).to(equal("coucou"))

    @pytest.mark.xfail(
        "numpy" not in sys.modules,
        raises=ModuleNotFoundError,
        reason="Requirements for data type handeling are not installed",
    )
    @pytest.mark.parametrize(
        ("suffix", "success"),
        (
            (".csv", True),
            (".parquet", True),
            (".xlsx", True),
            (".json", True),
            (".txt", False),
        ),
    )
    def test_datatypehandling(self, _clean, suffix, success):
        # Other types are not tested through S3Path directly. I assume that if
        # the tests in test_encoder_decoder/test_encoder_decoder.py are ok, then
        # only one type needs end2end testing to ensure coherence with S3Path
        import pandas as pd

        expect(S3Path.HANDLE_TYPE).to(be_true)

        data = pd.DataFrame(
            columns=["chien", "chat"], index=["a", "b"], data=[[0, 1], [2, 3]]
        )
        data_app = pd.DataFrame(
            columns=["cheval", "autruche"],
            index=["c", "d"],
            data=[[0, 1], [2, 3]],
        )
        path = S3Path(f"notebooks/dataset/data{suffix}")
        if success:
            path.write(data.to_dict() if suffix == ".json" else data)
            data2 = (
                path.read(index_col=0)
                if suffix in (".csv", ".xlsx")
                else path.read()
            )
            pd.testing.assert_frame_equal(
                pd.DataFrame.from_dict(data2) if suffix == ".json" else data2,
                data,
            )
            if suffix != ".json":
                path.write(
                    data_app,
                    append=True,
                    append_kwargs={"axis": 1},
                    read_kwargs={"index_col": 0}
                    if suffix in (".csv", ".xlsx")
                    else {},
                )
                data2 = (
                    path.read(index_col=0)
                    if suffix in (".csv", ".xlsx")
                    else path.read()
                )
                pd.testing.assert_frame_equal(
                    pd.concat((data, data_app), axis=1), data2
                )
        else:
            with pytest.raises(TypeError):
                path.write(data)

    @pytest.mark.xfail(
        "numpy" in sys.modules,
        reason="Requirements for data type handeling are installed",
    )
    def test_nodatahandeling(self):
        expect(S3Path.HANDLE_TYPE).to(be_false)
        S3Path.HANDLE_TYPE = True
        with pytest.raises(ModuleNotFoundError):
            S3Path()
        S3Path(handle_type=False)
        S3Path.HANDLE_TYPE = False
        path = S3Path()
        with pytest.raises(ModuleNotFoundError):
            path.handle_type = True
        with pytest.raises(ModuleNotFoundError):
            S3Path(handle_type=True)

    def test_self_save(self):
        path = S3Path("s3path.pkl")
        path.write(path)
        path2 = path.read(raise_if_unpickle_fails=True)
        expect(path2).to(equal(path))

    def test_sheets(self, prepare_test_sheets):
        path = S3Path("osef.csv")
        expect(path._sheets).to(be_none)
        expect(path.get_sheets()).to(equal([]))
        expect(path._sheets).not_to(be_none)

        for afile in prepare_test_sheets:
            expect(afile._sheets).to(be_none)
            sheets = afile.get_sheets()
            expect(afile._sheets).not_to(be_none)

            try:
                import pandas as pd  # noqa: F401
            except ModuleNotFoundError:
                expect(sheets).to(equal([]))
            else:
                expect(sheets).to(
                    equal(
                        ["Sheet1", "Sheet1_2"]
                        if self.CLIENT_TYPE != "boto"
                        else []
                    )
                )
