import pytest

from .s3path_testclass import TestS3Path


@pytest.mark.usefixtures("_minio_buckets")
class TestMinioPath(TestS3Path):
    CLIENT_TYPE = "minio"
