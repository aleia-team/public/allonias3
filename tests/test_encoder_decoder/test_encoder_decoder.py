import io
import json
import tempfile
import unittest
from copy import copy
from pathlib import Path
from time import sleep

import cloudpickle
import dill  # nosec
import h5py
import numpy as np
import pandas as pd
import pytest
from expects import contain, equal, expect

from allonias3 import S3Path
from allonias3.encoder_decoder.advanced import _Appender, _Decoder, _Encoder

dataframe1 = pd.DataFrame(
    index=["chien", "chat"],
    columns=["col1", "col2", "col3"],
    data=[[0, 1, 2], [3, 4, 5]],
)
dataframe1.index.name = "Unnamed: 0"
dataframe1_noheader = pd.DataFrame(
    index=["chat"], columns=["0", "1", "2"], data=[[3, 4, 5]]
)
dataframe1_noheader.index.name = "chien"
dataframe1_indexcol1 = pd.DataFrame(
    index=[0, 3],
    columns=["Unnamed: 0", "col2", "col3"],
    data=[["chien", 1, 2], ["chat", 4, 5]],
)
dataframe1_indexcol1.index.name = "col1"
dataframe2 = pd.DataFrame(
    index=["cheval", "hibou"],
    columns=["col1", "col2", "col3"],
    data=[[0, 10, 20], [30, 40, 50]],
)
dataframe2.index.name = "Unnamed: 0"
arr1 = dataframe1.to_numpy()
arr2 = dataframe2.to_numpy()
dict1 = dataframe1.T.to_dict()
dict2 = dataframe2.T.to_dict()
string1 = "chien"
string2 = "chat"
bstring1 = b"chien"
bstring2 = b"chat"
td = tempfile.TemporaryDirectory()
tf_name = Path(td.name) / "tmp.h5"
h5file = h5py.File(tf_name, "w")
h5file.create_dataset("data1", data=arr1)
h5file.close()
h5file = h5py.File(tf_name, "r")


class _EncodeDecodeTester(unittest.TestCase):
    skip = True
    suffix = None
    filename = None

    encoder = None
    decoder = None
    encoder_pickle = None
    decoder_pickle = None

    wrong_type_str = None
    wrong_suffix = None
    base_input = None
    to_append = None
    expected_encoded = None
    expected_encoded_pickle = None
    expected_decoded = None
    expected_decoded_pickle = None
    expected_appended = None
    expected_appended_pickle = None

    encoding_kwargs = None
    decoding_kwargs = None
    expected_decoded_with_encoding_kwargs = None
    expected_decoded_with_decoding_kwargs = None
    expected_appended_with_kwargs = None

    def file_content(self, content):
        if self.__class__.__name__.endswith("Json"):
            return {"Body": content}
        if isinstance(content, io.BufferedReader):
            content.seek(0)
            return {"Body": content}
        if isinstance(content, io.BytesIO):
            return {"Body": content}
        if isinstance(content, str):
            file_content = io.StringIO()
        else:
            file_content = io.BytesIO()

        file_content.write(content)
        file_content.seek(0)
        return {"Body": copy(file_content)}

    @classmethod
    def setup_class(cls) -> None:
        cls.suffix = cls.__name__.replace("TestEncoderDecoder", "").lower()
        cls.suffix = f".{cls.suffix}"
        cls.filename = S3Path(f"file{cls.suffix}")
        cls.encoder = _Encoder(cls.filename)
        cls.decoder = _Decoder(cls.filename)
        cls.appender = _Appender(cls.filename)
        cls.encoder_pickle = _Encoder(cls.filename, deactivate=True)
        cls.decoder_pickle = _Decoder(cls.filename, deactivate=True)
        cls.appender_pickle = _Appender(cls.filename, deactivate=True)

    @classmethod
    def assert_equal(cls, content1, content2):
        if isinstance(content1, pd.DataFrame):
            pd.testing.assert_frame_equal(content1, content2)
        elif isinstance(content1, pd.Series):
            pd.testing.assert_series_equal(content1, content2)
        elif isinstance(content1, np.ndarray):
            np.testing.assert_equal(content1, content2)
        elif isinstance(content1, (h5py.File, io.BufferedReader)):
            if isinstance(content1, io.BufferedReader):
                content1 = h5py.File(io.BytesIO(content1.read()))
                content2 = h5py.File(io.BytesIO(content2.read()))
            keys1 = list(content1.keys())
            keys2 = list(content2.keys())
            expect(keys1).to(equal(keys2))
            for key in keys1:
                np.testing.assert_equal(
                    np.array(content1[key]), np.array(content2[key])
                )
        else:
            expect(content1).to(equal(content2))

    def test_wrong_filename(self):
        if self.skip:
            self.skipTest("Abstract class")
        if self.wrong_suffix:
            with pytest.raises(TypeError) as e:
                _Encoder(S3Path(self.filename).with_suffix(self.wrong_suffix))(
                    self.base_input
                )
            expect(str(e.value)).to(
                contain(f" files, not to {self.wrong_suffix} files.")
            )

    def test_type_fail(self):
        if self.skip:
            self.skipTest("Abstract class")
        if self.wrong_type_str:
            with pytest.raises(TypeError) as e:
                self.encoder("chien")
            expect(str(e.value)).to(
                contain(
                    f"Can only write objects of type {self.wrong_type_str} to"
                    f" {self.suffix} files, not a <class 'str'>."
                )
            )

    def test_basic(self):
        sleep(0.01)
        if self.skip:
            self.skipTest("Abstract class")
        encoded = self.encoder(self.base_input)
        if isinstance(encoded, io.BytesIO):
            try:
                self.assert_equal(encoded.read(), self.expected_encoded.read())
                encoded.seek(0)
            except AssertionError:
                encoded.seek(0)
                self.expected_encoded.seek(0)
                self.assert_equal(
                    self.decoder({"Body": encoded}), self.expected_decoded
                )
                encoded.seek(0)
                self.expected_encoded.seek(0)
        else:
            self.assert_equal(encoded, self.expected_encoded)
        decoded = self.decoder(self.file_content(encoded))
        self.assert_equal(decoded, self.expected_decoded)

    def test_deactivate(self):
        if self.expected_encoded_pickle is None:
            return
        if self.skip:
            self.skipTest("Abstract class")
        encoded = self.encoder_pickle(self.base_input)
        self.assert_equal(encoded, self.expected_encoded_pickle)
        decoded = self.decoder_pickle(self.file_content(encoded))
        self.assert_equal(decoded, self.expected_decoded_pickle)
        if self.expected_appended_pickle is None:
            return
        appended_pickle = self.appender_pickle(
            self.expected_decoded, self.to_append
        )
        self.assert_equal(appended_pickle, self.expected_appended_pickle)

    def test_kwargs(self):
        if self.skip:
            self.skipTest("Abstract class")
        if self.encoding_kwargs is not None:
            encoded = self.encoder(self.base_input, **self.encoding_kwargs)
            decoded = self.decoder(self.file_content(encoded))
            self.assert_equal(
                decoded, self.expected_decoded_with_encoding_kwargs
            )
        if self.decoding_kwargs is not None:
            encoded = self.encoder(self.base_input)
            decoded = self.decoder(
                self.file_content(encoded), **self.decoding_kwargs
            )
            self.assert_equal(
                decoded, self.expected_decoded_with_decoding_kwargs
            )

    def test_append(self):
        if self.skip:
            self.skipTest("Abstract class")
        if self.to_append is not None:
            appended = self.appender(
                self.expected_decoded, self.to_append, append_kwargs={}
            )
            try:
                self.assert_equal(
                    copy(appended).read()
                    if isinstance(appended, io.BytesIO)
                    else appended,
                    copy(self.expected_appended).read()
                    if isinstance(self.expected_appended, io.BytesIO)
                    else self.expected_appended,
                )
            except AssertionError:
                self.assert_equal(
                    self.decoder({"Body": copy(appended)}),
                    self.decoder({"Body": copy(self.expected_appended)}),
                )
        else:
            with pytest.raises(
                TypeError, match="objects of type|Cannot append to"
            ):
                self.appender(self.base_input, string1)

    def test_append_kwargs(self):
        if self.skip:
            self.skipTest("Abstract class")
        if self.expected_appended_with_kwargs is not None:
            existing = (
                self.base_input
                if self.decoding_kwargs is None
                else self.decoder(
                    self.file_content(self.encoder(self.base_input)),
                    **self.decoding_kwargs,
                )
            )
            appended = self.appender(
                existing, self.to_append, write_kwargs=self.encoding_kwargs
            )
            try:
                self.assert_equal(
                    copy(appended).read()
                    if isinstance(appended, io.BytesIO)
                    else appended,
                    copy(self.expected_appended_with_kwargs).read(),
                )
            except AssertionError:
                self.assert_equal(
                    self.decoder({"Body": copy(appended)}),
                    self.decoder(
                        {"Body": copy(self.expected_appended_with_kwargs)}
                    ),
                )


class TestEncoderDecoderJson(_EncodeDecodeTester):
    @classmethod
    def setup_class(cls) -> None:
        super().setup_class()
        cls.skip = False
        cls.wrong_type_str = "<class 'dict'>"
        cls.wrong_suffix = ".txt"
        cls.base_input = dict1
        cls.to_append = dict2
        cls.expected_encoded = json.dumps(cls.base_input).encode()
        cls.expected_encoded_pickle = cloudpickle.dumps(cls.base_input)
        cls.expected_decoded = cls.base_input
        cls.expected_decoded_pickle = dill.loads(cls.expected_encoded_pickle)
        cls.expected_appended = copy(cls.base_input)
        cls.expected_appended.update(cls.to_append)
        cls.expected_appended = cls.encoder(cls.expected_appended)


class TestEncoderDecoderCSV(_EncodeDecodeTester):
    @classmethod
    def setup_class(cls) -> None:
        super().setup_class()
        cls.skip = False
        cls.wrong_type_str = (
            "(<class 'pandas.core.frame.DataFrame'>,"
            " <class 'pandas.core.series.Series'>)"
        )
        cls.wrong_suffix = ".txt"
        cls.base_input = dataframe1
        cls.to_append = dataframe2

        cls.encoding_kwargs = {"header": False}
        cls.decoding_kwargs = {"index_col": 1}
        cls.expected_decoded_with_encoding_kwargs = (
            dataframe1_noheader.reset_index()
        )
        cls.expected_decoded_with_decoding_kwargs = dataframe1_indexcol1

        cls.expected_encoded = io.BytesIO()
        cls.base_input.to_csv(cls.expected_encoded)
        cls.expected_encoded.seek(0)
        cls.expected_encoded_pickle = cloudpickle.dumps(cls.base_input)
        cls.expected_decoded = copy(cls.base_input)
        cls.expected_decoded.index.name = "Unnamed: 0"
        cls.expected_decoded = cls.expected_decoded.reset_index()
        cls.expected_decoded_pickle = dill.loads(cls.expected_encoded_pickle)
        append_input = cls.base_input.reset_index()
        cls.expected_appended = cls.encoder(
            pd.concat(
                [
                    append_input.rename(columns={"index": "Unnamed: 0"}),
                    cls.to_append,
                ]
            )
        )
        decoded_with_decoding_kwargs = cls.decoder(
            {"Body": cls.encoder(cls.base_input)}, **cls.decoding_kwargs
        )
        cls.expected_appended_with_kwargs = cls.encoder(
            pd.concat([decoded_with_decoding_kwargs, cls.to_append]),
            **cls.encoding_kwargs,
        )

    def test_encoding(self):
        filepath = S3Path(__file__).parent / "sample.csv"
        data = pd.read_csv(f"/{filepath}", encoding="ISO-8859-1", sep=";")
        mock_response_body = io.BytesIO()
        data.to_csv(mock_response_body, encoding="ISO-8859-1", sep=";")
        mock_response_body.seek(0)
        mock_response_body = {"Body": mock_response_body}
        pd.testing.assert_frame_equal(
            data,
            _Decoder(filepath, encoding="ISO-8859-1")(
                mock_response_body, sep=";", index_col=0
            ),
        )

        mock_response_body = io.BytesIO()
        data.to_csv(mock_response_body, encoding="ISO-8859-1", sep=";")
        mock_response_body.seek(0)

        expect(mock_response_body.read()).to(
            equal(
                _Encoder(filepath, encoding="ISO-8859-1")(data, sep=";").read()
            )
        )


class TestEncoderDecoderXLS(_EncodeDecodeTester):
    @classmethod
    def setup_class(cls) -> None:
        super().setup_class()
        cls.skip = False
        cls.wrong_type_str = (
            "(<class 'pandas.core.frame.DataFrame'>,"
            " <class 'pandas.core.series.Series'>,"
            " <class 'bytes'>, <class '_io.BytesIO'>)"
        )
        cls.wrong_suffix = ".txt"
        cls.base_input = copy(dataframe1)
        cls.to_append = dataframe2

        cls.encoding_kwargs = {"header": False}
        cls.decoding_kwargs = {"index_col": 1}
        cls.expected_decoded_with_encoding_kwargs = copy(dataframe1_noheader)
        cls.expected_decoded_with_decoding_kwargs = dataframe1_indexcol1
        # Excel engine will convert strings to integers, apparently...
        cls.expected_decoded_with_encoding_kwargs.columns = (
            cls.expected_decoded_with_encoding_kwargs.columns.astype(int)
        )
        cls.expected_decoded_with_encoding_kwargs = (
            cls.expected_decoded_with_encoding_kwargs.reset_index()
        )

        cls.expected_encoded = io.BytesIO()
        cls.base_input.to_excel(cls.expected_encoded)
        cls.expected_encoded.seek(0)
        cls.expected_encoded_pickle = cloudpickle.dumps(cls.base_input)
        cls.expected_decoded = copy(cls.base_input)
        cls.expected_decoded.index.name = "Unnamed: 0"
        cls.expected_decoded = cls.expected_decoded.reset_index()
        cls.expected_decoded_pickle = dill.loads(cls.expected_encoded_pickle)
        append_input = cls.base_input.reset_index()
        cls.expected_appended = cls.encoder(
            pd.concat(
                [
                    append_input.rename(columns={"index": "Unnamed: 0"}),
                    cls.to_append,
                ]
            )
        )
        decoded_with_decoding_kwargs = cls.decoder(
            {"Body": cls.encoder(cls.base_input)}, **cls.decoding_kwargs
        )
        cls.expected_appended_with_kwargs = cls.encoder(
            pd.concat([decoded_with_decoding_kwargs, cls.to_append]),
            **cls.encoding_kwargs,
        )


class TestEncoderDecoderXLSX(TestEncoderDecoderXLS):
    pass


class TestEncoderDecoderXLSM(TestEncoderDecoderXLS):
    pass


class TestEncoderDecoderODF(TestEncoderDecoderXLS):
    pass


class TestEncoderDecoderODS(TestEncoderDecoderXLS):
    pass


class TestEncoderDecoderODT(TestEncoderDecoderXLS):
    pass


class TestEncoderDecoderH5(_EncodeDecodeTester):
    @classmethod
    def setup_class(cls) -> None:
        super().setup_class()
        cls.skip = False
        cls.wrong_type_str = (
            "(<class 'bytes'>, <class 'h5py._hl.files.File'>,"
            " <class '_io.BufferedReader'>)"
        )
        cls.wrong_suffix = None
        cls.base_input = h5file
        cls.to_append = None

        cls.encoding_kwargs = None
        cls.decoding_kwargs = None
        cls.expected_decoded_with_encoding_kwargs = None
        cls.expected_decoded_with_decoding_kwargs = None

        cls.expected_encoded = Path(tf_name).open("rb")  # noqa: SIM115
        cls.expected_encoded_pickle = None
        cls.expected_decoded = cls.base_input
        cls.expected_decoded_pickle = None
        cls.expected_appended = None
        cls.expected_appended_with_kwargs = None


class TestEncoderDecoderParquet(_EncodeDecodeTester):
    @classmethod
    def setup_class(cls) -> None:
        super().setup_class()
        cls.skip = False
        cls.wrong_type_str = (
            "(<class 'pandas.core.frame.DataFrame'>,"
            " <class 'pandas.core.series.Series'>)"
        )
        cls.wrong_suffix = ".txt"
        cls.base_input = dataframe1
        cls.to_append = dataframe2

        cls.expected_encoded = io.BytesIO()
        cls.base_input.to_parquet(cls.expected_encoded)
        cls.expected_encoded.seek(0)
        cls.expected_encoded_pickle = cloudpickle.dumps(cls.base_input)
        cls.expected_decoded = cls.base_input
        cls.expected_decoded_pickle = dill.loads(cls.expected_encoded_pickle)
        cls.expected_appended = cls.encoder(
            pd.concat([cls.base_input, cls.to_append])
        )


class TestEncoderDecoderNPY(_EncodeDecodeTester):
    @classmethod
    def setup_class(cls) -> None:
        super().setup_class()
        cls.skip = False
        cls.wrong_type_str = "<class 'numpy.ndarray'>"
        cls.wrong_suffix = ".txt"
        cls.base_input = arr1
        cls.to_append = arr2

        cls.expected_encoded = cloudpickle.dumps(cls.base_input)
        cls.expected_encoded_pickle = cls.expected_encoded
        cls.expected_decoded = cls.base_input
        cls.expected_decoded_pickle = dill.loads(cls.expected_encoded_pickle)
        cls.expected_appended = cls.encoder(
            np.concatenate([cls.base_input, cls.to_append])
        )


class TestEncoderDecoderTXT(_EncodeDecodeTester):
    @classmethod
    def setup_class(cls) -> None:
        super().setup_class()
        cls.skip = False
        cls.base_input = string1
        cls.to_append = string2

        cls.expected_encoded = cls.base_input.encode("utf-8")
        cls.expected_encoded_pickle = cls.expected_encoded
        cls.expected_decoded = cls.base_input
        cls.expected_decoded_pickle = cls.base_input
        cls.expected_appended = cls.encoder(f"{cls.base_input}{cls.to_append}")
        cls.expected_appended_pickle = cls.expected_appended


class TestEncoderDecoderNone(_EncodeDecodeTester):
    @classmethod
    def setup_class(cls) -> None:
        super().setup_class()
        cls.skip = False
        cls.base_input = None
        cls.to_append = string2

        cls.expected_encoded = b""
        cls.expected_encoded_pickle = cloudpickle.dumps(None)
        cls.expected_decoded = ""
        cls.expected_decoded_pickle = dill.loads(cls.expected_encoded_pickle)
        cls.expected_appended = cls.encoder(cls.to_append)

    def test_append_none(self):
        # Test also the other way around : appending None to something
        # (default test appends something to None)
        self.assert_equal(
            self.appender(self.to_append, self.base_input, write_kwargs={}),
            self.to_append.encode("utf-8"),
        )


class TestEncoderDecoderPKL(_EncodeDecodeTester):
    @classmethod
    def setup_class(cls) -> None:
        super().setup_class()
        cls.skip = False
        cls.base_input = bstring1

        cls.expected_encoded = cloudpickle.dumps(cls.base_input)
        cls.expected_encoded_pickle = cls.expected_encoded
        cls.expected_decoded = cls.base_input
        cls.expected_decoded_pickle = cls.base_input
