import pandas as pd
import pytest
from expects import contain, equal, expect

from allonias3 import S3Path

dataframe1 = pd.DataFrame(
    index=["chien", "chat"],
    columns=["col1", "col2", "col3"],
    data=[[0, 1, 2], [3, 4, 5]],
)


@pytest.mark.usefixtures("_skipabstract")
class TestS3Path:
    CLIENT_TYPE = None

    def test_use_extension(self, _clean):
        path = S3Path("notebooks/dataset/test.csv.txt", use_extension=".csv")
        path.write(dataframe1)
        pd.testing.assert_frame_equal(path.read(index_col=0), dataframe1)

    def test_invalid_extension(self, _clean):
        path = S3Path(
            "notebooks/dataset/test.csv.txt", use_extension=".toto.tutu"
        )
        with pytest.raises(TypeError) as error:
            path.write(dataframe1)
        expect(str(error.value)).to(contain("Invalid file extension"))

    def test_use_extension_activated_then_deactivated(self, _clean):
        path = S3Path("notebooks/dataset/test.csv.txt", use_extension=".csv")
        path.write(dataframe1)
        path.use_extension = None
        expect(path.read()).to(
            equal(",col1,col2,col3\nchien,0,1,2\nchat,3,4,5\n")
        )

    def test_use_extension_deactivated_then_activated(self, _clean):
        path = S3Path("notebooks/dataset/test.csv")
        path.write(dataframe1)
        path.use_extension = ".txt"
        expect(path.read()).to(
            equal(",col1,col2,col3\nchien,0,1,2\nchat,3,4,5\n")
        )

    def test_use_extension_deactivated_error(self, _clean):
        path = S3Path("notebooks/dataset/test.csv.txt")
        with pytest.raises(TypeError) as error:
            path.write(dataframe1)
        expect(str(error.value)).to(contain("Can only write objects of type"))


@pytest.mark.usefixtures("_minio_buckets")
class TestMinioPath(TestS3Path):
    CLIENT_TYPE = "minio"


@pytest.mark.usefixtures("_boto_buckets")
class TestBotoPath(TestS3Path):
    CLIENT_TYPE = "boto"
