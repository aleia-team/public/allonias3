import json

import numpy as np
import pandas as pd
from dateutil import parser as dateparser

from allonias3.encoder_decoder.advanced import JSONEncoder, json_obj_hook

datetimes = [
    dateparser.parse(f"2020-01-01 {h if h > 9 else '0' + str(h)}:00:00")
    for h in range(24)
]
index = list(
    pd.date_range("2020-01-01", "2020-01-02", freq="h", inclusive="left")
)


def test_encode_decode_dataframe():
    dataframe = pd.DataFrame(
        index=index,
        columns=["Integers", "timedeltas", "datetimes", "dates"],
        data=np.array(
            [
                range(24),
                [pd.Timedelta(hours=h) for h in range(0, 48, 2)],
                datetimes,
                [d.date() for d in datetimes],
            ]
        ).T,
    )
    encoded = json.dumps(dataframe, cls=JSONEncoder)
    decoded = json.loads(encoded, object_hook=json_obj_hook)
    pd.testing.assert_frame_equal(dataframe, decoded)


def test_encode_decode_series():
    series = pd.Series(index=index, data=range(24), name="chien")
    encoded = json.dumps(series, cls=JSONEncoder)
    decoded = json.loads(encoded, object_hook=json_obj_hook)
    pd.testing.assert_series_equal(series, decoded)

    s_unnamed = pd.Series(index=index, data=range(24))
    encoded = json.dumps(s_unnamed, cls=JSONEncoder)
    decoded = json.loads(encoded, object_hook=json_obj_hook)
    pd.testing.assert_series_equal(s_unnamed, decoded)
