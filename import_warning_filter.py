"""Sphinx extension used to make Import Error treated as errors instead of
warnings.

This is necessary because to build the documentation, Sphinx tries to import
the classes and functions it is supposed to build the docstrings for. If there
is an import error here, likely there is a problem in a docstring, or worse, and
we need to know it. But by default it only produces a warning, making our
pipeline succeed. This extension will catch those warnings and turn them into
errors.

We could have used the '-W' flag to raise all warnings, but there are also
warnings that we do not really care about that would produce a pipeline
failure.
"""

import logging

from sphinx.application import Sphinx


class ImportErrorHandler(logging.Handler):
    """Custom logging handler to catch import-related warnings."""

    def emit(self, record):
        if "Failed to import" in record.msg:
            raise RuntimeError(
                f"Sphinx build failed due to import error: {record.msg}"
            )


def setup(app: Sphinx):
    """Setup function to attach the custom import error handler."""
    app.connect("builder-inited", attach_handler)


def attach_handler(_):
    """Attach the custom handler to Sphinx's logger."""
    handler = ImportErrorHandler()
    handler.setLevel(logging.WARNING)  # Only intercept warnings or higher
    logger = logging.getLogger("sphinx")
    logger.addHandler(handler)
