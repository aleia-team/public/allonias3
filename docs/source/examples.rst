.. _examples:

##########
 Examples
##########

***********
 Notebooks
***********

The following notebooks provide working examples that you can copy-paste
in Aleia Platform's jupyter lab. We recommand exploring them all in the order
they appear below.

.. nbgallery::
   :caption: See some examples in notebooks :

   examples/notebooks/basic_usage.ipynb
   examples/notebooks/io.ipynb
   examples/notebooks/use_locally.ipynb
