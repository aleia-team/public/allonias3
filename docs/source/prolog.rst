.. only:: html

    .. role:: raw-html(raw)
        :format: html

    .. note::

        | Download :download:`file <{{ docname }}>`