..
   allonias3 documentation master file

#######################################
 Welcome to AllOnIAS3's documentation!
#######################################

.. toctree::
   :hidden:
   :caption: Table of Contents:

   examples
   basic_usage
   logging
   long_running_tasks
   io
   versioning
   object_type
   importing
   docstrings

.. mdinclude:: ../../README.md