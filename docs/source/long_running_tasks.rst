.. _background:

##############################################
 Execute long running tasks in the background
##############################################

This is not really specific to AllOnIAS3, but it is a useful thing to know
when working with notebooks.

Imagine that you have a long running task, and that none of the following
tasks in your workflow depend on it. You would like to run it in the background
and continue executing your next cells.

For the sake of argument, let us assume that we have a lot of files in our
dataset folder, and that we would like to delete them all on a loop. That could
take several minutes.

Here is how you could launch the deletion in the background and continue working:

.. code-block:: python

    from allonias3 import S3Path
    import pandas as pd
    import logging
    import concurrent.futures

    # logger to be able to see the deletion progress
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler()
    # Create a formatter and set it for the handler (optional, for more detailed output)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    def delete_bckgr(content_):
        for c in content_:
            c.rm()

    dataset_dir = S3Path("notebooks/dataset")
    content = dataset_dir.content()
    executor = concurrent.futures.ThreadPoolExecutor()
    future = executor.submit(delete_bckgr, content)  # starts the deletion
    ...
    # The next line will block until the end of the deletion, so execute it
    # last. It is not even recquired here, since delete_bckgr does not return
    # anything.
    future.result()