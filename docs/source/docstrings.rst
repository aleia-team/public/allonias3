#######################
 AllOnIAS3 Docstrings
#######################

*******
 Paths
*******

----------
 BasePath
----------

.. autoclass:: allonias3.base_path.BasePath
   :members:

--------
 S3Path
--------

.. autoclass:: allonias3.s3_path.S3Path
   :members:

----------
 BotoPath
----------

.. autoclass:: allonias3.boto.boto_path.BotoPath
   :members:

-----------
 MinioPath
-----------

.. autoclass:: allonias3.minio.minio_path.MinioPath
   :members:

*******************
 Advanced features
*******************

-----------------
 Encoder-Decoder
-----------------

=======
 Basic
=======

.. automodule:: allonias3.encoder_decoder.basic
   :members:
   :special-members:
   :private-members:

==========
 Advanced
==========

.. automodule:: allonias3.encoder_decoder.advanced
   :members:
   :special-members:
   :private-members:

-------------------
 Helpers and tools
-------------------

.. automodule:: allonias3.helpers.class_property
   :members:

.. automodule:: allonias3.helpers.getattr_safe_property
   :members:

.. automodule:: allonias3.helpers.responses
   :members:

.. automodule:: allonias3.helpers.utils
   :members:

.. automodule:: allonias3.helpers.enums
   :members:

.. automodule:: allonias3.helpers.nblog
   :members:
