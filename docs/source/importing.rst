.. _import:

##############################
 Import a python file from S3
##############################

Sometimes you might want to externalize some functions and variables in a
python file and import it in your notebook. However, since the files on S3
are not on the running machine, it is not straightforward. You can achieve it
by using :obj:`~allonias3.base_path.BasePath.import_from_s3`.

In the following example, we will import the function :inlinepython:`imported_function`
that we defined as

.. code-block:: python

    def imported_function(a):
        return 2 * a

in the file **to_import.py**, located in **notebooks/notebook/to_import.py** (it could
be anywhere really).

.. code-block:: python

    from allonias3 import S3Path

    path = S3Path("notebooks/notebook/to_import.py")
    path.import_from_s3()
    from to_import import imported_function
    imported_function(2)  # returns 4
