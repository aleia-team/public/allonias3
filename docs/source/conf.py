import sys
from pathlib import Path
from datetime import datetime

sys.path.insert(0, str(Path("../../").resolve()))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "allonias3"
copyright = f"{datetime.now().year}, AllOnIA"
author = "AllOnIA"
source_suffix = [".md", ".rst"]
release = "0.1.0"
# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

rst_prolog = """
.. role:: inlinepython(code)
   :language: python
"""

extensions = [
    "sphinx.ext.napoleon",
    "sphinx.ext.duration",
    "sphinx.ext.doctest",
    "sphinx.ext.autodoc",
    "sphinx_autodoc_typehints",
    "sphinx.ext.autosummary",
    "sphinx.ext.intersphinx",
    "sphinx_mdinclude",
    "nbsphinx",
    "sphinx_copybutton",
    "import_warning_filter"
]

sphinx_gallery_conf = {
    "plot_gallery": "False",
    "download_all_examples": False,
    "compress_images": ("images", "thumbnails"),
}
nbsphinx_execute = "never"
nbsphinx_prolog = (
    r"""
{% set docname = "../../" + env.doc2path(env.docname, base=None) %}
"""
    + Path("prolog.rst").read_text()
)
templates_path = ["_templates"]
nbsphinx_thumbnails = {
    "examples/notebooks/io": "_static/jupyter-logo.png",
    "examples/notebooks/basic_usage": "_static/jupyter-logo.png",
    "examples/notebooks/use_locally": "_static/jupyter-logo.png",
}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "pydata_sphinx_theme"
exclude_patterns = ["prolog.rst"]
html_static_path = ["_static"]
html_logo = "allonia-logo.svg"

# html_css_files = [
#     'css/custom.css'
# ]
autosummary_generate = True

autodoc_mock_imports = [
    "pandas",
    "numpy",
    "sklearn",
    "public",
    "h5py",
    "hdf5plugin",
    "dill",
    "typeguard",
    "pillow",
]
html_context = {"default_mode": "light"}
html_sidebars = {
    "**": [
        "search-field.html",
        "globaltoc.html",
        "relations.html",
        "sourcelink.html",
    ]
}

intersphinx_mapping = {
    "pandas": ("https://pandas.pydata.org/docs/", None),
    "numpy": ("https://numpy.org/doc/stable/", None),
    "sklearn": ("https://scikit-learn.org/stable", None),
    "python": ("https://docs.python.org/3", None),
    "h5py": ("https://docs.h5py.org/en/stable", None),
    "dill": ("https://dill.readthedocs.io/en/latest", None)
}

napoleon_google_docstring = True
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = True
napoleon_use_ivar = True
napoleon_use_param = True
napoleon_use_rtype = True
napoleon_preprocess_types = False
napoleon_type_aliases = True
napoleon_attr_annotations = True
always_use_bars_union = True
