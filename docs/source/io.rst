.. _io:

#####################
 Reading and writing
#####################

To read or write to or from a :obj:`~allonias3.s3_path.S3Path`, use
:obj:`~allonias3.base_path.BasePath.read` and :obj:`~allonias3.base_path.BasePath.write`
respectivelly.

********************
 Special data types
********************

If you installed the optional dependencies for handeling special data types
by doing

.. code-block:: bash

    pip install allonias3[datatypehandler]


then
:obj:`~allonias3.base_path.BasePath.read` supports being given special data
types **based on the file extension**. If not, only strings, bytes and streams
can be given to write.

For example, to write and read a :obj:`pandas.DataFrame` object, without the
special datatype handeling, one would do:

.. code-block:: python

    from allonias3 import S3Path
    import pandas as pd
    import io

    path = S3Path("notebooks/dataset/data.csv", handle_type=False)
    data = pd.DataFrame(...)
    path.write(io.BytesIO(data))
    data = pd.DataFrame(path.read())


Note that the file is a csv, but it could actually have any extension.
With special data type handeling, one would do

.. code-block:: python

    from allonias3 import S3Path
    import pandas as pd

    path = S3Path("notebooks/dataset/data.csv")
    data = pd.DataFrame(...)
    path.write(data)
    data = path.read()

In that case, the file **must be** a csv or parquet.

Special data type handeling is enabled by default if all the required packages
are found, else it is disabled.

You can choose to disable/enable it by:

* setting the class attribute :obj:`~allonias3.base_path.BasePath.HANDLE_TYPE`
  to True or False, which will affect all :obj:`~allonias3.s3_path.S3Path`
  instances
* setting :obj:`~allonias3.base_path.BasePath.handle_type` of one instance of the
  class to True or False
* create an instance of the class by passing :inlinepython:`handle_type=False/True` to its
  constructor

Trying to enable special type handeling when the appropriate packages are not
installed will immediately raise an error.

=======================================
 Extensions and their associated types
=======================================

When special data type handeling is enabled, you can not write any type of
object to any type of file.

Here are the restrictions when **writing** data:

* **.csv** or **.parquet**: :inlinepython:`content` must be a
  :obj:`~pandas.DataFrame` or :obj:`~pandas.Series`,
  :inlinepython:`**write_kwargs` are passed to
  :obj:`~pandas.DataFrame.to_csv` or
  or :obj:`~pandas.DataFrame.to_parquet`
* **.xls**, **.xlsx**, **.xlsm**, **.xlsb**, **.odf**, **.ods** or
  **.odt**: :inlinepython:`content` must be a
  :obj:`~pandas.DataFrame`, :obj:`~pandas.Series`, :obj:`bytes` or
  :obj:`~io.BytesIO`. :inlinepython:`**write_kwargs` are passed to
  :obj:`~pandas.DataFrame.to_excel`
* **.json**: :inlinepython:`content` must be a :obj:`dict`,
  :inlinepython:`**write_kwargs` are passed to :obj:`json.dumps`.
* **.npy**: :inlinepython:`content` must be a :obj:`~numpy.ndarray`,
  which will be pickled, `write_kwargs` will be ignored.
* **.h5**: :inlinepython:`content` must be an open :obj:`h5py.File`
  object. ALWAYS use :inlinepython:`handle_type=True` when saving HDF5
  files, as they can not be pickled.
  :inlinepython:`**write_kwargs` will be ignored.

Here are the restrictions when **reading** data:

* **.csv** : will return a :obj:`~pandas.DataFrame`.
  :inlinepython:`**kwargs` are passed to :obj:`~pandas.read_csv`.
* **.parquet** : will return a :obj:`~pandas.DataFrame`.
  :inlinepython:`**kwargs` are passed to :obj:`~pandas.read_parquet`.
* **.json** : will return a :obj:`dict`.
  :inlinepython:`**kwargs` are passed to :obj:`json.loads`
* **.npy** : will return a :obj:`~numpy.ndarray`.
  :inlinepython:`**kwargs` will be ignored.
* **.h5** : will return an open :obj:`h5py.File` object. ALWAYS use
  :inlinepython:`handle_type=True` when reading HDF5 files, they can not
  be (un)pickled. :inlinepython:`**kwargs` will be ignored.

Any other type can be saved to any other extension. For example, you can
always saved a pickled object to a pkl file, or text to a txt file.

***********
 Appending
***********

You can choose to append data to an existing file instead of overwriting it.
**This is not the default behavior!** you must explicitely give :inlinepython:`append=True`
to :obj:`~allonias3.base_path.BasePath.write`:

.. code-block:: python

    from allonias3 import S3Path
    import pandas as pd

    path = S3Path("notebooks/dataset/data.csv")
    data = pd.DataFrame(index=[0, 1], [[0, 1], [2, 3]])
    # Append rows
    data2 = pd.DataFrame(index=[2, 3], data=(2 * data).values)
    path.write(data)
    path.write(data2, append=True, read_kwargs={"index_col": 0}, append_kwargs={"axis": 0})
    path.read()  #  [[0, 1], [2, 3], [0, 2], [4, 6]]
    # Append columns
    data2 = pd.DataFrame(columns=[2, 3], data=(2 * data).values)
    path.write(data2, append=True, read_kwargs={"index_col": 0}, append_kwargs={"axis": 1})
    path.read()  # [[0, 1, 0, 2], [2, 3, 4, 6]]

Note the importance of :inlinepython:`read_kwargs={"index_col": 0}`: it tells
the decoder to load the existing dataframe using the first columns as index. If
we did not use it, we would append a dataframe of 2 columns to a dataframe of 3
columns.Also, the previous example assumes that handling of special data type is
enabled. One can append :obj:`pandas.DataFrame` objects to csv, parquet, or
all xls-like extensions, :obj:`numpy.ndarray` objects to npy files,
:obj:`dict` objects to json files, and text or bytes or any file that already
contains text or bytes respectivelly. A line break,
:obj:`pandas.concat`, :obj:`numpy.concatenate` or :obj:`dict.update` is
used to append. If handling of special data type is disabled, one can only
append text or bytes.

********************
 Some special cases
********************

======================
 Encoding and Windows
======================

If you have, for example, a dataframe containing French accents, and you write
it to S3, you should use the 'cp1252' encoding if you are to download the file
on a Windows machine later, otherwise you might lose the accents :

.. code-block:: python

    from allonias3 import S3Path
    import pandas as pd
    df = pd.DataFrame([["ééééàçà"], ["âêôüè"]])
    path = S3Path("notebooks/dataset/with_accents.csv")
    path.write(
        df,
        write_kwargs={"encoding": "cp1252"},
    )
    path.read(encoding="cp1252", index_col=0)

Note however that by doing that, you might have other problems if reading the
file on Unix... so you might want to save two files with two encodings ('UTF-8'
should be enough for Unix) if you are to use the file on Unix and Windows.

===========================================
 Writing a Excel file with multiple sheets
===========================================

.. code-block:: python

    import pandas as pd
    from allonias3 import S3Path

    lst = [['tom', 25], ['krish', 30], ['nick', 26], ['juli', 22]]
    df = pd.DataFrame(lst, columns =['Name', 'Age'])
    df2 = pd.DataFrame(lst, columns =['name2', 'Age2'])
    df3 = pd.DataFrame(lst, columns =['name3', 'Age3'])
    output = io.BytesIO()

    outputName = 'test3.xlsx'
    with pd.ExcelWriter(output) as writer:
            df.to_excel(writer, sheet_name="df", index=False)
            df2.to_excel(writer, sheet_name="df2", index=False)
            df3.to_excel(writer, sheet_name="df3", index=False)
    output.seek(0)
    S3Path("notebooks/dataset/exceldata.xlsx").write(output)
    S3Path("notebooks/dataset/exceldata.xlsx").read(sheet_name="df2")



==================
 Reading an image
==================

UNTESTED !

.. code-block:: python

    import cv2
    import numpy as np
    from allonias3 import S3Path
    import matplotlib.pyplot as plt

    path = S3Path("notebooks/dataset/image.png")
    img = cv2.imdecode(
        np.frombuffer(path.read(), dtype=np.uint8),
        cv2.IMREAD_UNCHANGED
    )
    plt.imshow(img)
    plt.show()