.. _logging:

#####################
 See AllOnIAS3 logs
#####################

By default, in a notebook, the root logger does not have any handlers configured
to output log messages. In JupyterLab, you need to add a handler, such as a
StreamHandler, to ensure that log messages are output to the console.

Add this cell at the beginning of your notebook, and you should see all the logs:

.. code-block:: python

    from allonias3 import nb_log

    logger = nb_log("INFO")

You will most likely not use the returned :inlinepython:`logger` object, so
you can also do

.. code-block:: python

    from allonias3 import nb_log

    nb_log("INFO")