.. _versioning:

#############
 Versioning
#############


Objects on AllOnIA's S3 can be created either on the **persistent** or
**non-persistent** bucket. On the **persistent** bucket all objects are verisoned,
none are on the **non-persistent** bucket.

Put simply, versioning allows to keep previous state of a S3 object. When such
an object is deleted, instead a special version is created, called a
**delete marker**. When a versioned object is overwriten, instead a new version
is created. more information can be found on the `official S3 documentation`_.
On our plateform however, in order not to accumulate to many versions, deletion
is by default permanent : all versions are deleted. To have the S3 behavior
of creating a delete marker, one must give :inlinepython:`permanently=False`
to :obj:`~allonias3.base_path.BasePath.rm`.

.. _official S3 documentation: https://docs.aws.amazon.com/AmazonS3/latest/userguide/Versioning.html

***************************
 Interacting with versions
***************************

=========
 Listing
=========

One can see the versions of an existing object on S3 through the
:obj:`~allonias3.base_path.BasePath.versions` method. By default, it returns
a list of uuids, ordered by decreasing creation dates (index 0 contains the most
recent version). Also, by default, delete markers are not listed. One can choose
to see them too by specifying :inlinepython:`include_deleted=True`:

 .. code-block:: python

    from allonias3 import S3Path

    path = S3Path("notebooks/dataset/data.csv")
    path.is_file()  # True or False
    data = path.read()
    # or
    data = ...
    path.write(data)
    path.versions()  # list the existing versions



==========
 Deleting
==========

As mentioned above, contrary to S3's default behavior, deleting an object using
:obj:`~allonias3.base_path.BasePath.rm` will delete all its versions, unless
:inlinepython:`permanently=False` is given:

 .. code-block:: python

    from allonias3 import S3Path

    path = S3Path("notebooks/dataset/data.csv")
    path.is_file()  # True or False
    data = path.read()
    # or
    data = ...
    path.write(data)
    path.versions()  # contain N versions
    path.rm(permanently=False)  # create a delete marker
    path.versions()  # should still contain N versions, as delete marker are ignored
    versions = path.versions(include_deleted=True)  # should contain N+1 versions
    path.rm(version_id=versions[0])  # delete the latest version (the delete marker in that case)
    path.rm()  # delete all versions
    path.versions()  # empty list



===========
 Restoring
===========

There is currently no method to restore a previous version, but you can
read a specific version:



 .. code-block:: python

    from allonias3 import S3Path

    path = S3Path("notebooks/dataset/data.csv")
    data = path.read(version_id=...)
    # or
    data = path.read(revision=2)  # read the second oldest version, ignores delete markers