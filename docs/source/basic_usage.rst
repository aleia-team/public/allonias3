.. _basicusage:

#############
 Basic Usage
#############

The class :obj:`~allonias3.s3_path.S3Path` allows the user to
interact with objects on S3 the same way he/she would use :obj:`pathlib.Path`:

 .. code-block:: python

    from allonias3 import S3Path

    path = S3Path("notebooks/dataset/data.csv")
    path.is_file()  # True or False
    data = path.read()
    # or
    data = ...
    path.write(data)
    path.versions()  # list the existing versions
    path.object_type  # should be "dataset"
    dataset_dir = path.parent  # notebooks/dataset
    dataset_dir.is_dir()  # True
    files = dataset.content()  # list files in the directory

You can find advanced example of reading and writing data here : :ref:`io`

You can also check out :ref:`logging` to see how to activate log messages
in your notebook.