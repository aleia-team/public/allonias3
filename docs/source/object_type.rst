.. _object_type:

#############
 Object Type
#############

The object type is set automatically depending on the location of the
object on S3. If the type can not be deduced from the path, it will be
"unknown". You can set it yourself to any value present in
:obj:`~allonias3.helpers.enums.ObjectTypeEnum`.

.. code-block:: python

    from allonias3 import S3Path

    path = S3Path("notebooks/myfolder/test.csv")
    path.object_type  # will return 'unknown'

    path = S3Path("notebooks/notebook/myfile.py")
    path.object_type  # will return 'notebook'

Note that one existing file can only have one type: the following code
will raise :obj:`TypeError`

.. code-block:: python

    from allonias3 import S3Path

    path = S3Path("notebooks/myfolder/test.csv")  # type is None
    path.write(...)  # type becomes 'unknown'
    path.object_type = "dataset"
    path.write(...)  # will raise TypeError